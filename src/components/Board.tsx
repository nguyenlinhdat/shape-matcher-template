import React, { useState, useEffect } from "react";
import Cell from "./Cell";
import "./Board.css";

export interface ItemType {
  id: number;
  img: string;
  stat: string;
}

const Board: React.FC = () => {
  const [items, setItems] = useState<ItemType[]>(
    [
      { id: 1, img: "/img/triangle.jpg", stat: "" },
      { id: 1, img: "/img/triangle.jpg", stat: "" },
      { id: 2, img: "/img/triangleBlue.jpg", stat: "" },
      { id: 2, img: "/img/triangleBlue.jpg", stat: "" },
      { id: 3, img: "/img/redSquare.png", stat: "" },
      { id: 3, img: "/img/redSquare.png", stat: "" },
      { id: 4, img: "/img/blueSquare.png", stat: "" },
      { id: 4, img: "/img/blueSquare.png", stat: "" },
      { id: 5, img: "/img/blueCircle.jpg", stat: "" },
      { id: 5, img: "/img/blueCircle.jpg", stat: "" },
      { id: 6, img: "/img/redCircle.png", stat: "" },
      { id: 6, img: "/img/redCircle.png", stat: "" },
      { id: 7, img: "/img/redHexagon.png", stat: "" },
      { id: 7, img: "/img/redHexagon.png", stat: "" },
      { id: 8, img: "/img/blueHexagon.png", stat: "" },
      { id: 8, img: "/img/blueHexagon.png", stat: "" },
    ].sort(() => Math.random() - 0.5)
  );

  const [prev, setPrev] = useState<number>(-1);
  // states...
  useEffect(() => {
    // Initialize the game board with random shapes and colors
  }, []);

  const check = (current: number) => {
    if (items[current].id == items[prev].id) {
      items[current].stat = "correct";
      items[prev].stat = "correct";
      setItems([...items]);
      setPrev(-1);
    } else {
      items[current].stat = "wrong";
      items[prev].stat = "wrong";
      setItems([...items]);
      setTimeout(() => {
        items[current].stat = "";
        items[prev].stat = "";
        setItems([...items]);
        setPrev(-1);
      }, 1000);
    }
  };

  const handleCellClick = (index: number) => {
    // Reveal cell, check for matches, update game state, and handle game completion
    if (prev === -1) {
      items[index].stat = "active";
      setItems([...items]);
      setPrev(index);
    } else {
      check(index);
    }
  };

  return (
    <div className="board">
      {items.map((item: any, index: number) => (
        <Cell
          key={index}
          item={item}
          id={index}
          handleCellClick={handleCellClick}
        />
      ))}
    </div>
  );
};

export default Board;
