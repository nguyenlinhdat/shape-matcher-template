import React from "react";
import { ItemType } from "./Board";
import "./Cell.css";

interface CellProps {
  // Your code here
  item: ItemType;
  id: number;
  handleCellClick: (index: number) => void;
}

const Cell: React.FC<CellProps> = (props: CellProps) => {
  // Render cell with shape and color, use CSS to style based on shape and color.
  const { item, id, handleCellClick } = props;
  const itemClass = item.stat ? " active " + item.stat : "";
  return (
    <div className={"card" + itemClass} onClick={() => handleCellClick(id)}>
      <img src={item.img} alt="" className="img" />
    </div>
  );
};

export default Cell;
